package com.techprimers.security.springsecurityauthserver;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/rest/hello")
public class HelloResource {


    @GetMapping("/principal")
    public Principal user(Principal principal) {
    	
        return principal;
    }
    
    @GetMapping("/authentication")
    public Authentication userAuthentication(Authentication auth) {
    	
        return auth;
    }
    
    @GetMapping
    public String hello() {
        return "Hello World";
    }

}
