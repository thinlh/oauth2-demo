package com.techprimers.security.springsecurityclient.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

	@GetMapping("/")
	public String home(){
		return "index";
	}
	
	@GetMapping("/secure")
	public String secure(Model model, Authentication auth){
		model.addAttribute("userName", auth.getName()).addAttribute("grants", auth.getAuthorities());
		return "secure";
	}
	
	
}
