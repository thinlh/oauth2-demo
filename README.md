# Spring Security OAuth Example

- `spring-security-client` - Client Project which has the UI 
- `spring-security-auth-server` - Has the Authorization Server and Resource Server
- `http://localhost:8082/ui` - REST end point for UI which will take you to the secure URI `http://localhost:8082/secure` after logging into the auth server `http://localhost:8081/auth/login`
- `application.yml` set up client info(port, context-path) and congfig security oauth2(clientId, clientSecret, clientSecret, accessTokenUri, userAuthorizationUri, userInfoUri)
link giới thiệu oauth2:
https://viblo.asia/p/introduction-to-oauth2-3OEqGjDpR9bL